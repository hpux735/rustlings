// quiz1.rs
// This is a quiz for the following sections:
// - Variables
// - Functions

fn calculate_apple_price(apples: i32) -> i32 {
    // If you buy more than 40 apples, they're 1 rustbuck each
    if apples > 40 {
        return apples
    }
    // Otherwise, they're 2.
    else {
        return apples * 2;
    }
}

// Don't modify this function!
#[test]
fn verify_test() {
    let price1 = calculate_apple_price(35);
    let price2 = calculate_apple_price(65);

    assert_eq!(70, price1);
    assert_eq!(65, price2);
}
